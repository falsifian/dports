--- vendor/github.com/hashicorp/packer-plugin-sdk/pathing/config_file_unix.go.orig	2021-12-20 11:25:55 UTC
+++ vendor/github.com/hashicorp/packer-plugin-sdk/pathing/config_file_unix.go
@@ -1,4 +1,4 @@
-// +build darwin freebsd linux netbsd openbsd solaris
+// +build darwin freebsd linux netbsd openbsd solaris dragonfly
 
 package pathing
 
